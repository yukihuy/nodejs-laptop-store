const fs = require('fs');//Module hổ trợ đọc file
const url = require('url');//The url module provides utilities for URL resolution and parsing. It can be accessed using:
const http = require('http');//Module hổ trợ giao thưc(protocol) http

const json = fs.readFileSync(`${__dirname}/data/data.json`, 'utf8');
const laptopData = JSON.parse(json);

const server = http.createServer((req, res) => {

    const pathName = url.parse(req.url, true).pathname;
    //req :Request Listener ==> Lấy Request 
    const id = url.parse(req.url, true).query.id;

    if (pathName === '/products' || pathName === '/') {

        res.writeHead(200, { 'Content-type': 'text/html' });

        fs.readFile(`${__dirname}/templates/template-overview.html`, 'utf-8', (err, data) => {
            let overviewOutput = data;
            //readFile() để đọc dữ liệu từ file
            fs.readFile(`${__dirname}/templates/template-card.html`, 'utf-8', (err, data) => {
                //join() để nói các chuối trong mảng lại với nhau
                const cardsOutput = laptopData.map(el => replaceTemplate(data, el)).join('');
                
                overviewOutput = overviewOutput.replace(/{%CARDS%}/g, cardsOutput);
                res.end(overviewOutput);
            });
        });


    }
    else if (pathName === '/laptop' || id < laptopData.length) {

        res.writeHead(200, { 'Content-type': 'text/html' });

        fs.readFile(`${__dirname}/templates/template-laptop.html`, 'utf8', (err, data) => {

            const laptop = laptopData[id];
           // console.log(laptop);
            const output = replaceTemplate(data, laptop);
            res.end(output);

        });

    }
    // IMAGES
    //Image test RegExp ==> Kiểm tra ký hiệu đuôi của .jpg, .jpeg, .png, .gif trong 
    else if ((/\.(jpg|jpeg|png|gif)$/i).test(pathName)) {
        fs.readFile(`${__dirname}/data/img${pathName}`, (err, data) => {
            res.writeHead(200, { 'Content-type': 'image/jpg' });
            res.end(data);
        });
    }
    else {
        res.writeHead(400, { 'Content-type': 'text/html' });
        res.end('URL was the not found on the server');
    }

});

server.listen(1337, '127.0.0.1', () => {
    console.log(' Listening for request now');
});

function replaceTemplate(originalHtml, laptop) {

    let output = originalHtml.replace(/{%PRODUCTNAME%}/g, laptop.productName);
    output = output.replace(/{%IMAGE%}/g, laptop.image);
    output = output.replace(/{%SCREEN%}/g, laptop.screen);
    output = output.replace(/{%PRICE%}/g, laptop.price);
    output = output.replace(/{%CPU%}/g, laptop.cpu);
    output = output.replace(/{%STORAGE%}/g, laptop.storage);
    output = output.replace(/{%RAM%}/g, laptop.ram);
    output = output.replace(/{%DESCRIPTION%}/g, laptop.description);
    output = output.replace(/{%ID%}/g, laptop.id);
    return output;

}